import * as fs from "fs";
import { IUser, UserRole } from "./db/employees";
import { normalizePassword } from "./utils/PassUtil";
const IP = require("ip");

export interface IServerSettings {
    port: number;
}

export interface IFServiceSettings {
    port: number;
}

export interface IRServiceSettings {
    port: number;
}

export interface IAdminsSettings {
    username: string;
    password: string;
}

export interface ISettingsData {
    rService?: IRServiceSettings;
    server: IServerSettings;
    admins: Array<IAdminsSettings>;
}

class MainSettings {

    private _ip: string = "";
    public get ip(): string { return this._ip; }

    private _data: ISettingsData;
    public get data(): ISettingsData { return this._data; }

    public get server(): IServerSettings { return this._data.server; }

    public get rService(): IRServiceSettings { return this._data.rService; }

    public get admins(): Array<IAdminsSettings> { return this._data.admins; }

    private _adminsList: Array<IUser> = [];
    public get adminsList() { return this._adminsList; }

    private _adminsMap: Map<string, IUser> = new Map();
    public get adminMap() { return this._adminsMap; }

    /**
     * Чтение базовых настроек.
     */
    constructor() {
        this._ip = IP.address();
    }

    public read(cb: (err?: NodeJS.ErrnoException) => void): void {
        const data = fs.readFileSync('config.json', 'utf8');
        try {
            this._data = JSON.parse(data);
        } catch (e) {
            return cb(e);
        }
        try {
            this.createAdminsMap();
        } catch (e) {
            return cb(e);
        }
        return cb();
    }

    private createAdminsMap(): void {
        if (this.admins) {
            this.admins.forEach(admin => {
                if (admin.username && admin.password) {
                    const user: IUser = this.createAdminData(admin);
                    this.adminMap.set(admin.username, user);
                    this._adminsList.push(user);
                }
            });
        }
    }

    private createAdminData(admin: IAdminsSettings): IUser {
        const user: IUser = {
            user: admin.username,
            role: UserRole.ADMIN,
            divisions: null, // all
            password: normalizePassword(admin.password),
            email: '',
            token: ''
        }
        return user;
    }
}

export let Settings = new MainSettings();
