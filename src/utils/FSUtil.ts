import * as path from "path";
import * as fs from "fs";

const mkdirRequrs = require('mkdir-recursive');

const getSize = require('get-folder-size');

export interface IMakeDirResult {}

export const mkdirp = function (dir: string): Promise<IMakeDirResult> {
    return new Promise<IMakeDirResult>((resolve, reject) => {
        mkdirRequrs.mkdir(dir, (err) => {
            if (err) { if (err.message.indexOf('EEXIST') !== -1) resolve(); else reject(err)}
            else resolve();
          });
    });
}

interface IGetFilesSortedByCDateOptions {
    reverse?: boolean
}

export const getFilesSortedByCDate = function (dir: string, callback: (files: Array<string>) => void, options?: IGetFilesSortedByCDateOptions): void {
    let files: Array<string>;
    fs.readdir(dir, (err, result) => {
        if (err) throw err;
        files = result;

        files.sort((a: string, b: string) => {
            const sa = fs.statSync(dir + b).mtime.getTime();
            const sb = fs.statSync(dir + a).mtime.getTime();
            let dif: number = options && options.reverse == true ? sb - sa : sa - sb;
            return sa - sb;
        });
        if (callback) callback(files);
    });
}

export const getDirectorySize = function (dir: string, callback: Function): void {
    getSize(dir, (err: any, size: number) => {
        if (err) { throw err; }
        if (callback) callback(size);
    });
}

export const getBytesByStr = function (str: string): number {
    let result: number = 0;
    try {
        if (str && str.length > 0) {
            if (str.length > 1 && str.indexOf("K") == str.length - 1 || str.indexOf("k") == str.length - 1) {
                result = parseFloat(str.substring(0, str.length - 1)) * 1000;
            } else if (str.length > 1 && (str.indexOf("M") == str.length - 1 || str.indexOf("m") == str.length - 1)) {
                result = parseFloat(str.substring(0, str.length - 1)) * 1000000
            } else if (str.length > 1 && str.indexOf("G") == str.length - 1 || str.indexOf("g") == str.length - 1) {
                result = parseFloat(str.substring(0, str.length - 1)) * 1000000000;
            } else if (str.length > 1 && str.indexOf("T") == str.length - 1 || str.indexOf("t") == str.length - 1) {
                result = parseFloat(str.substring(0, str.length - 1)) * 1000000000000;
            } else result = parseFloat(str);
        }
    } catch (e) { }
    return result;
}