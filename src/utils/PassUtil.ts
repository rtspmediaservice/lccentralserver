import * as MD5 from "md5";

const S: string = String.fromCharCode(55, 12, 45);

export const getPass = (p: string): string => {
    return MD5(p);
}

export const getS = (): string => {
    return S;
}

export const validPass = (p): boolean => {
    return true; //p && p.indexOf(S) != -1;
}

export const normalizePassword = (p: string): string => {
    return getPass(p);
}