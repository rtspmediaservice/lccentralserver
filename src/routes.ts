import * as express from "express";
import { IDivision, DBDivisions } from "./db/divisions";
import { IUser, DBEmployees, IUserDivisions } from "./db/employees";
import { Settings } from "./settings";
import { Base64 } from 'js-base64';
import { resolve } from "bluebird";

export class Routes {

    constructor(private _expressApp: express.Application, private _dbEmployees: DBEmployees, private _dbDivisions: DBDivisions) {
        this.configureRoutes();
    }

    private verify(req: express.Request) {
        return new Promise<IUser>((resolve, reject) => {
            const token: string = req.headers['token'] as string;
            if (token) {
                this._dbEmployees.checkUser({ token: token })
                    .then((user) => {
                        resolve(user);
                    })
                    .catch((err) => {
                        reject(err);
                    })
            } else reject(new Error("Token is wrong"));
        });
    }

    configureRoutes() {
        /**
         * REGISTRATION
         */
        this._expressApp.post("/api/registration", (req: express.Request, res: express.Response) => {
            this.verify(req)
                .then((user: IUser) => {
                    /*this._dbEmployees.createUser({ user: req.body.user, password: req.body.password, email: req.body.email }, (err, doc) => {
                        res.send(this.createResponseData<IUser>(err ? { message: err.message, code: 100 } : null, doc))
                    });*/
                    this._dbEmployees.createUser(req.body)
                        .then((doc: IUser) => {
                            res.send(this.createResponseData<ILoginAnswer>(null, doc));
                        })
                        .catch((reason: Error) => {
                            res.send(this.createResponseData<IUser>({ message: reason ? reason.message : "Unknown error", code: 201 }, null));
                        });
                })
                .catch((err) => {
                    res.send(this.createResponseData<IUser>({ message: err ? err.message : "Verify user error", code: 501 }, null));
                })
        });

        /**
         * LOGIN
         */
        this._expressApp.post("/api/login", (req: express.Request, res: express.Response) => {
            this._dbEmployees.login(req.body)
                .then((doc: IUser) => {
                    res.send(this.createResponseData<ILoginAnswer>(null, doc));
                })
                .catch((reason: Error) => {
                    res.send(this.createResponseData<IUser>({ message: reason ? reason.message : "Unknown error", code: 201 }, null));
                });
        });

        /**
         * CHECKUSER
         */
        this._expressApp.get("/api/checkuser", (req: express.Request, res: express.Response) => {
            this.verify(req)
                .then((user: IUser) => {
                    const userName: string = req.query['user'] as string;

                    if (userName) {
                        this._dbEmployees.checkUser({ user: userName })
                            .then((doc: IUser) => {
                                res.send(this.createResponseData<ILoginAnswer>(null, doc));
                            })
                            .catch((reason: Error) => {
                                if (reason.message === DBEmployees.ERR_USER_NOT_FOUND) res.send(this.createResponseData<IUser>({ message: "Wrong token", code: 100 }, null));
                                else res.send(this.createResponseData<IUser>({ message: reason ? reason.message : "Unknown error", code: 201 }, null));
                            });
                    } else res.send(this.createResponseData<ILoginAnswer>(null, user));
                })
                .catch((err) => {
                    res.send(this.createResponseData<IUser>({ message: err ? err.message : "Verify user error", code: 501 }, null));
                })
        });

        /**
         * USERS
         */
        this._expressApp.get("/api/users", (req: express.Request, res: express.Response) => {
            this.verify(req)
                .then((user) => {
                    this._dbEmployees.users()
                        .then((docs: Array<IUser>) => {
                            res.send(this.createResponseData<any>(null, docs));
                        })
                        .catch((reason: Error) => {
                            res.send(this.createResponseData<IUser>({ message: reason ? reason.message : "Unknown error", code: 201 }, null));
                        });
                })
                .catch((err) => {
                    res.send(this.createResponseData<IUser>({ message: err ? err.message : "Verify user error", code: 501 }, null));
                })
        });

        /**
         * EDITUSER
         */
        this._expressApp.post("/api/edituser", (req: express.Request, res: express.Response) => {
            this.verify(req)
                .then((user) => {
                    this._dbEmployees.editUser(req.body)
                        .then((doc: IUser) => {
                            res.send(this.createResponseData<ILoginAnswer>(null, doc));
                        })
                        .catch((reason: Error) => {
                            if (reason.message === DBEmployees.ERR_USER_NOT_FOUND) res.send(this.createResponseData<IUser>({ message: "Wrong user", code: 101 }, null));
                            else res.send(this.createResponseData<IUser>({ message: reason ? reason.message : "Unknown error", code: 201 }, null));
                        });
                })
                .catch((err) => {
                    res.send(this.createResponseData<IUser>({ message: err ? err.message : "Verify user error", code: 501 }, null));
                })
        });

        /**
         * LOGOUT
         */
        this._expressApp.post("/api/logout", (req: express.Request, res: express.Response) => {
            this.verify(req)
                .then((user) => {
                    this._dbEmployees.logout(user)
                        .then((doc: IUser) => {
                            res.send(this.createResponseData<ILoginAnswer>(null, { user: doc.user }));
                        })
                        .catch((reason: Error) => {
                            res.send(this.createResponseData<IUser>({ message: reason ? reason.message : "Unknown error", code: 201 }, null));
                        });
                })
                .catch((err) => {
                    res.send(this.createResponseData<IUser>({ message: err ? err.message : "Verify user error", code: 501 }, null));
                })
        });

        /**
         * GET DIVISIONS BY USER
         */
        this._expressApp.get("/api/divisions", (req: express.Request, res: express.Response) => {
            this.verify(req)
                .then((user: IUser) => {
                    const userName: string = req.query['user'] as string;
                    if (userName) {
                        this._dbEmployees.checkUser({ user: userName })
                            .then((doc: IUser) => {
                                this._dbDivisions.getDivisionsByUserRights(doc.divisions)
                                    .then((docs: IDivision[]) => {
                                        res.send(this.createResponseData<IDivision[]>(null, docs));
                                    })
                                    .catch((reason: Error) => {
                                        res.send(this.createResponseData<IUser>({ message: reason ? reason.message : "Unknown error", code: 201 }, null));
                                    })
                            })
                            .catch((reason: Error) => {
                                if (reason.message === DBEmployees.ERR_USER_NOT_FOUND) res.send(this.createResponseData<IUser>({ message: "Wrong token", code: 100 }, null));
                                else res.send(this.createResponseData<IUser>({ message: reason ? reason.message : "Unknown error", code: 201 }, null));
                            });
                    } else {
                        this._dbDivisions.getDivisionsByUserRights(user.divisions)
                            .then((docs: IDivision[]) => {
                                res.send(this.createResponseData<IDivision[]>(null, docs));
                            })
                            .catch((reason: Error) => {
                                res.send(this.createResponseData<IUser>({ message: reason ? reason.message : "Unknown error", code: 201 }, null));
                            })
                    }
                })
                .catch((err) => {
                    res.send(this.createResponseData<IUser>({ message: err ? err.message : "Verify user error", code: 501 }, null));
                })
        });

        /**
         * GET ALL DIVISIONS
         */
        this._expressApp.get("/api/alldivisions", (req: express.Request, res: express.Response) => {
            this.verify(req)
                .then((user) => {
                    this._dbDivisions.getAllDivisions()
                        .then((docs: IDivision[]) => {
                            res.send(this.createResponseData<IDivision[]>(null, docs));
                        })
                        .catch((reason: Error) => {
                            res.send(this.createResponseData<IUser>({ message: reason ? reason.message : "Unknown error", code: 201 }, null));
                        })
                })
                .catch((err) => {
                    res.send(this.createResponseData<IUser>({ message: err ? err.message : "Verify user error", code: 501 }, null));
                })
        });

        /**
         * CREATE DIVISION
         */
        this._expressApp.post("/api/createdivision", (req: express.Request, res: express.Response) => {
            this._dbDivisions.createDivision(req.body.data)
                .then((value) => {
                    res.send(this.createResponseData<IDivision>(null, value));
                })
                .catch((reason: Error) => {
                    res.send(this.createResponseData<IDivision>({ message: reason ? reason.message : "Unknown error", code: 400 }, null));
                });
        });
    }

    private createResponseData<T>(err: IResponseError, doc: T): IResponse<T> {
        return { error: err, data: doc };
    }
}

export interface ILoginAnswer {
    user: string;
    token?: string;
    ws?: number;
}

export interface IResponseError {
    message: string;
    code: number;
}

export interface IResponse<T> {
    error: IResponseError;
    data: T;
}