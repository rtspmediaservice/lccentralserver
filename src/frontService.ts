import * as http from "http";
import * as WebSocket from "ws";
import { logger } from "./logger";
import { Registrator } from "./registratorService";
import { Settings } from "./settings";

export class FrontService {

    private _server: WebSocket.Server;
    public get server(): WebSocket.Server {
        return this._server;
    }

    constructor(private _httpService: http.Server) { }

    initialize() {
        this._server = new WebSocket.Server({
            //port: Settings.server.port,
            server: this._httpService,

        }, () => {
            console.log('start');
        });

        this._server.on("connection", (ws: WebSocket) => {
            const front = new Front(ws);
            Front.pool.push(front);
        });
    }
}

export class Front {

    public static RT_INIT: string = "init";
    public static RT_PLAY_PREVIEW: string = "play-m";
    public static RT_PLAY_HIRES: string = "play-h";
    public static RT_STOP_PREVIEW: string = "stop-m";
    public static RT_STOP_HIRES: string = "stop-h";
    /**
     * <division, <cam, count>>
     */
    public static playCamsM: Map<string, Map<string, number>> = new Map();
    public static playCamsH: Map<string, Map<string, number>> = new Map();
    public static map: Map<string, Front> = new Map();
    public static pool: Array<Front> = [];

    /** uuid регистратора */
    public get uuid(): string { return this._uuid; }
    private _uuid: string;

    private _camsM: Array<string> = [];
    get camsM(): Array<string> { return this._camsM; }

    private _camsH: Array<string> = [];
    get camsH(): Array<string> { return this._camsH; }

    /**
     * Удаление всех фронтов с заданным регистратором.
     * Этот метод вызывается при дисконнекте регистратора.
     */
    public static deleteRegistrator(uuid: string) {
        const front = Front.map.get(uuid);
        if (front) {
            front.stopAll();
            const index = Front.pool.indexOf(front);
            if (index !== -1) Front.pool.splice(index, 1);
            Front.map.delete(uuid);
        }
    }

    constructor(private _ws: WebSocket) {
        this._ws.on("close", (e: any) => {
            this.detach(); 
        });
        this._ws.on("message", (data) => {
            this.execCmd(data);
        });
        this._ws.on("error", (e: any) => {
            logger.error(`[F-SERV]::[WS] Error. [${e}]`);
        });
        this._ws.on("open", (e: any) => {
            console.log("open: ", e);
        });
    }

    private execCmd(data: WebSocket.Data): void {
        if (!data) return;
        let cmd: any;
        try {
            cmd = JSON.parse(String(data));
        } catch (e) {
            logger.error(`[F-SERV]::Bad command. {${data}}`);
            return;
        }
        switch (cmd.action) {
            case Front.RT_INIT: {
                this.init(cmd);
                break;
            }
            case Front.RT_PLAY_PREVIEW: {
                this.playCmdM(cmd);
                break;
            }
            case Front.RT_PLAY_HIRES: {
                this.playCmdH(cmd);
                break;
            }
            case Front.RT_STOP_PREVIEW: {
                this.stopCmdM(cmd);
                break;
            }
            case Front.RT_STOP_HIRES: {
                this.stopCmdH(cmd);
                break;
            }
        }
    }

    private playCmdM(cmd: ICommand<IFrontRequest>) {
        const camsM: string[] = cmd.data.cams;
        this.pushCams(camsM, this._camsM);
        this.play(camsM, true);
    }

    private playCmdH(cmd: ICommand<IFrontRequest>) {
        const camsH: string[] = cmd.data.cams;
        this.pushCams(camsH, this._camsH);
        this.play(camsH);
    }

    private pushCams(cams: string[], array: string[]) {
        for (const cam of cams) {
            if (array.indexOf(cam) === -1) array.push(cam);
        }
    }

    public stopAll() {
        this.stop(this._camsM, true);
        this.stop(this._camsH);
    }

    private stopCmdM(cmd: ICommand<IFrontRequest>) {
        const camsM = cmd.data.cams;
        this.spliceCams(camsM, this._camsM);
        this.stop(camsM, true);
    }

    private stopCmdH(cmd: ICommand<IFrontRequest>) {
        const camsH = cmd.data.cams;
        this.spliceCams(camsH, this._camsH);
        this.stop(camsH);
    }

    private spliceCams(cams: string[], array: string[]) {
        for (const cam of cams) {
            const index = array.indexOf(cam);
            if (index === -1) array.splice(index, 1);
        }
    }

    public play(camNames: string[], preview: boolean = false) {

        const map: Map<string, Map<string, number>> = preview ? Front.playCamsM : Front.playCamsH;

        logger.info(`[F-SERV]::#play ${preview ? 'preview' : 'hires'}. [${camNames ? camNames.toString() : ''}]`);
        let requestPlayCams: Array<string> = [];
        let div = map.get(this._uuid);

        if (!div) {
            div = new Map();
            map.set(this._uuid, div);
        }
        for (const camName of camNames) {
            let playCam = div.get(camName);
            if (playCam && playCam > 0) {
                ++playCam;
            } else {
                playCam = 1;
                requestPlayCams.push(camName);
            }
            console.log(camName, preview ? "p" : 'h', playCam)
            div.set(camName, playCam);
        }

        if (requestPlayCams.length > 0) {
            const registrator = Registrator.map.get(this._uuid);
            if (registrator) registrator.play(requestPlayCams, preview);
        }
    }

    public stop(camNames: string[], preview: boolean = false) {

        const map: Map<string, Map<string, number>> = preview ? Front.playCamsM : Front.playCamsH;

        logger.info(`[F-SERV]::#stop ${preview ? 'preview' : 'hires'}. [${camNames ? camNames.toString() : ''}]`);
        let requestStopCams: Array<string> = [];
        let div = map.get(this._uuid);

        if (!div) return;

        for (const camName of camNames) {
            let stopCam = div.get(camName);
            if (stopCam > 0) {
                --stopCam;
                if (stopCam === 0) requestStopCams.push(camName);
            }
            div.set(camName, stopCam);
        }

        if (requestStopCams.length > 0) {
            const registrator = Registrator.map.get(this._uuid);
            if (registrator) registrator.stop(requestStopCams, preview);
        }
    }

    private init(cmd: ICommand<IFrontRequest>) {
        const uuid = cmd.data.uuid;
        logger.info(`[F-SERV]::#init {${uuid}}`);
        if (uuid !== this._uuid) {
            this.stop(this._camsM, true);
            this.stop(this._camsH);
            this._uuid = uuid;
        }
        this.attach();
    }

    private attach() {
        logger.info(`[F-SERV]::#attach {${this._uuid}}`);
        Front.map.set(this._uuid, this);
    }

    private detach() {
        this.stop(this._camsM, true);
        const registrator = Registrator.map.get(this._uuid);
        //if (registrator) {
            //registrator.stop(this._camsM, true);
            //registrator.stop(this._camsH);
        //}

        logger.info(`[F-SERV]::#detach {${this._uuid}}`);
        Front.map.delete(this._uuid);
        /*this.stopM(this._camsM);
        this.stopH(this._camsH);*/
        this.dispose();
    }

    private removeFromPool() {
        const index = Front.pool.indexOf(this);
        if (index !== -1) Front.pool.splice(index, 1);
    }

    public dispose() {
        this.removeFromPool();
        if (this._ws) {
            this._ws.terminate();
            this._ws = null;
        }
    }
}

export interface ICommand<T> {
    action: string;
    data: T;
}

export interface IFrontRequest {
    uuid: string;
    division: string;
    cams: Array<string>;
}