import * as http from "http";
import * as WebSocket from "ws";
import { logger } from "./logger";
import { Settings } from "./settings";
import { Front } from "./frontService";

export class RegistratorService {

    private _server: WebSocket.Server;
    public get server(): WebSocket.Server {
        return this._server;
    }

    constructor(private _httpService: http.Server) { }

    initialize() {
        this._server = new WebSocket.Server({
            port: Settings.rService.port,
            server: this._httpService
        });

        this._server.on("connection", (ws: WebSocket) => {
            const registrator = new Registrator(ws);
            Registrator.pool.push(registrator);
        });
    }

    /**
     * @param uuid // UUID Регистратора / подразделения
     * ...
     */
    private play(uuid: string, cams: string[]) {
        const registrator = Registrator.map.get(uuid);
        if (registrator) {
            registrator.play(cams);
        }
    }
}

export class Registrator {

    public static map: Map<string, Registrator> = new Map();
    public static pool: Array<Registrator> = [];

    public static RT_INIT: string = "init";
    public static RT_PLAY_PREVIEW: string = "play-m";
    public static RT_PLAY_HIRES: string = "play-h";
    public static RT_STOP_PREVIEW: string = "stop-m";
    public static RT_STOP_HIRES: string = "stop-h";

    private _uuid: string;
    public get uuid(): string { return this._uuid; }

    constructor(private _ws: WebSocket) {
        this._ws.on("close", (e: any) => {
            this.detach();
        });
        this._ws.on("message", (data) => {
            this.execCmd(data);
        });
        this._ws.on("error", (e: any) => {
            logger.error(`[R-SERV]::[WS] error. [${e.message}]`);
        });
    }

    private execCmd(data: WebSocket.Data): void {
        if (!data) return;
        let cmd: any;
        try {
            cmd = JSON.parse(String(data));
        } catch (e) {
            logger.error(`[R-SERV]::[WS] bad command. [${String(data)}]`);
            return;
        }

        switch (cmd.action) {
            case "init": {
                this.init(cmd);
                break;
            }
        }
    }

    public play(cams: string[], preview: boolean = false) {
        logger.info(`[R-SERV]::#play ${preview ? 'preview' : 'hires'}. [${cams ? cams.toString() : ''}]`);
        const command: ICommand<IRTMPCommand> = {action: preview ? Registrator.RT_PLAY_PREVIEW : Registrator.RT_PLAY_HIRES, data: {cams: cams}};
        this._ws.send(JSON.stringify(command), (err) => {
            if (err) logger.error(`[R-SERV]::#play ${preview ? 'preview' : 'hires'} error. ${err.message}`);
        })
    }

    public stop(cams: string[], preview: boolean = false) {
        logger.info(`[R-SERV]::#stop ${preview ? 'preview' : 'hires'}. [${cams ? cams.toString() : ''}]`);
        const command: ICommand<IRTMPCommand> = {action: preview ? Registrator.RT_STOP_PREVIEW : Registrator.RT_STOP_HIRES, data: {cams: cams}};
        this._ws.send(JSON.stringify(command), (err) => {
            if(err) logger.error(`[R-SERV]::#stop ${preview ? 'preview' : 'hires'} error. ${err.message}`);
        })
    }

    private init(cmd: ICommand<IRegistrator>) {
        this._uuid = cmd.data.uuid;
        logger.info(`[R-SERV]::#init {${this._uuid}}`);
        this.attach();
    }

    private attach() {
        logger.info(`[R-SERV]::#attach {${this._uuid}}`);
        Registrator.map.set(this._uuid, this);

        // Возобновление трансляции после перезапуска регистратора
        const front = Front.map.get(this._uuid);
        if (front) {
            this.play(front.camsM, true);
            this.play(front.camsH, true);
        }
    }

    private detach() {
        logger.info(`[R-SERV]::#detach {${this._uuid}}`);
        Front.deleteRegistrator(this._uuid);
        Registrator.map.delete(this._uuid);
        this.dispose();
    }

    private removeFromPool() {
        const index = Registrator.pool.indexOf(this);
        if (index !== -1) Registrator.pool.splice(index, 1);
    }

    public dispose() {
        this.removeFromPool();
        if (this._ws) {
            this._ws.terminate();
            this._ws = null;
        }
    }
}

export interface ICommand<T> {
    action: string;
    data: T;
}

export interface IRTMPCommand {
    cams: Array<string>;
}

export interface IRegistrator {
    uuid: string;
    data: IRegistratorSettings;
}

export interface IH264Quality {
    preset?: string;
    bitrate?: {
        min: string;
        max: string;
        buf: string;
        pass: number;
    }
}

export interface IOutputSettings {
    folder?: string;
    segmentTime?: number;
    quality?: IH264Quality;
    capacity?: string;
}

export interface ICameraSource {
    src: string;
    output: IOutputSettings;
}

export interface IDivisionSettings {
    name: string;
}

export interface ICameraSettings {
    name?: string;
    preview?: ICameraSource;
    full?: ICameraSource;
}

export interface IServerSettings {
    port: string;
}

export interface ICentralServerSettings {
    address: string;
}

export interface ITranslatorServerSettings {
    host: string;
    port: string;
}

export interface IFFMPEGSettings {
    ffmpegPath: string;
    ffprobePath: string;
    flvtoolPath: string;
}

export interface IRegistratorSettings {
    uuid?: string;
    centralServer: ICentralServerSettings;
    translatorServer: ITranslatorServerSettings;
    server: IServerSettings;
    cameras: Array<ICameraSettings>;
    ffmpeg: IFFMPEGSettings;
    division?: IDivisionSettings;
}