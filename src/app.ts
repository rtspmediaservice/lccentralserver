// =================================================================================================
//
//	LC Central Server
//	Copyright Grebennikov Evgenii.
//
// =================================================================================================

import * as express from "express";
import * as errorHandler from "errorhandler";
import * as bodyParser from "body-parser";
import * as path from "path";
import * as http from "http";

import { logger } from "./logger";
import { Settings } from "./settings";
import { DBEmployees } from "./db/employees";
import { DBDivisions } from "./db/divisions";
import { Routes } from "./routes";
import { RegistratorService } from "./registratorService";
import { FrontService } from "./frontService";

export class App {

    public static PUBLIC: string = "public";
    public static UPLOADS: string = "uploads";

    private _expressApp: express.Application;
    public get expressApp(): express.Application {
        return this._expressApp;
    }

    private _httpService: http.Server;
    public get httpService(): http.Server {
        return this._httpService;
    }

    private _frontService: FrontService;
    private _registratorService: RegistratorService;

    private _publicDir: string;

    private _dbEmployees: DBEmployees;
    private _dbDivisions: DBDivisions;

    private _routes: Routes;

    constructor() {

        process.on('uncaughtException', (e) => {
            logger.error(e.message);
        });

        Settings.read((err) => {
            if (err) return logger.error("Error reading \"config.json\"");

            this.initializeEnv();
        });
    }

    private loadEmployeesDb(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this._dbEmployees = new DBEmployees((err?: Error) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            });
        });
    }

    private loadDivisionsDb(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this._dbDivisions = new DBDivisions((err?: Error) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            });
        });
    }

    private createAdminsIfNeed(): Promise<any> {
        return this._dbEmployees.createAdminsIfNeed(Settings.adminsList);
    }

    private initializeEnv(): void {

        this._publicDir = `${__dirname}${path.sep}${App.PUBLIC}`;

        this.loadEmployeesDb()
            .then(() => {
                this.loadDivisionsDb()
                    .then(() => {
                        this.createAdminsIfNeed()
                        .then(() => { this.runServer(); })
                        .catch(() => { this.runServer(); })
                    }).catch(() => {
                        logger.error("Error loading divisions db.");
                    })
            })
            .catch(() => {
                logger.error("Error loading employees db.");
            });
    }

    private runServer(): void {
        this._expressApp = express();
        this._expressApp.set("port", Settings.server.port);

        this._expressApp.use(errorHandler());
        this._expressApp.use(bodyParser.json());
        this._expressApp.use(bodyParser.urlencoded({
            extended: true
        }));

        // Добавление разрешенных заголовков
        this._expressApp.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
            res.header("Access-Control-Allow-Headers", "content-type, token, Cache-control, content-length, accept");
            res.header("Access-Control-Max-Age", "86400");
            if (req.method === "OPTIONS") {
                res.end();
            } else {
                next();
            }
        });
        
        this._expressApp.use(express.static(this._publicDir));
        
        this._httpService = http.createServer(this._expressApp);

        this._httpService.addListener("close", () => {
            logger.error(`Http server is stopped.`);
        });
        this._httpService.addListener("error", (err: Error) => {
            logger.error(`Http server is stopped. ${err}`);
        });

        this._httpService.listen(this._expressApp.get("port") || 8999, () => {
            logger.info(`Server running at http://localhost:${this._expressApp.get("port")} in ${this._expressApp.get("env")} mode`);
            logger.info("Press CTRL-C for stoping server\n");
            
            this.runRegistratorService();
            this.runFrontService();
        });

        this._routes = new Routes(this._expressApp, this._dbEmployees, this._dbDivisions);
    }

    private runRegistratorService() {
        this._registratorService = new RegistratorService(this._httpService);
        this._registratorService.initialize();
    }

    private runFrontService() {
        this._frontService = new FrontService(this._httpService);
        this._frontService.initialize();
    }
}

export const app = new App();