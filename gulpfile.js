var gulp = require('gulp');
var ts = require('gulp-typescript');
var minify = require('gulp-minify');
var sourcemaps = require('gulp-sourcemaps');
 
gulp.task('default', function () {
    return gulp.src('src/**/*.ts')
        .pipe(sourcemaps.init())
        .pipe(ts({
            target: "ES6",
            module: "commonjs",
            strict: false,
            noImplicitAny: false,
            removeComments: true/*,
            outFile: "index.js"*/
        }))/*.pipe(minify({
            ext:{
                src:'-debug.js',
                min:'.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))*/
        .pipe(sourcemaps.write('.', { sourceRoot: './', includeContent: true }))
        .pipe(gulp.dest('./dist'));
});